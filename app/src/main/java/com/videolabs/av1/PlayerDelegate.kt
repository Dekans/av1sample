/**
 * **************************************************************************
 * PlayerDelegate.kt
 * ****************************************************************************
 * Copyright © 2019 Videolabs
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 * ***************************************************************************
 */
package com.videolabs.av1

import kotlinx.android.synthetic.main.activity_fullscreen.*
import kotlinx.coroutines.*
import org.videolan.libvlc.LibVLC
import org.videolan.libvlc.MediaPlayer

@ExperimentalCoroutinesApi
class PlayerDelegate(private val activity: FullscreenActivity) : CoroutineScope by activity {

    private lateinit var mediaplayer: MediaPlayer

    private suspend fun setup() {
        val libvlc = withContext(Dispatchers.IO) { LibVLC(activity.applicationContext) }
        mediaplayer = MediaPlayer(libvlc)
        mediaplayer.attachViews(activity.videolayout, null, false, false)
        mediaplayer.updateVideoSurfaces()
    }

    fun release() = launch(Dispatchers.IO) {
        mediaplayer.release()
        activity.cancel()
    }

    fun start() = launch {
        if (!this@PlayerDelegate::mediaplayer.isInitialized) setup()
        withContext(Dispatchers.IO) {
            mediaplayer.playAsset(activity.applicationContext, "sample.webm")
        }
    }

    fun updateSize() {
        mediaplayer.updateVideoSurfaces()
    }
}